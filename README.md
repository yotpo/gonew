[godoc.org]: https://godoc.org/gitlab.com/yotpo/gonew "godoc.org"
[issues]: https://gitlab.com/yotpo/gonew/issues "Github issues"
[v2]: https://gitlab.com/yotpo/gonew/tree/v2/ "V2"

[![Build Status](https://travis-ci.org/YotpoLtd/gonew.png?branch=master)](https://travis-ci.org/YotpoLtd/gonew)

Gonew V2
========

This is Gonew V2. It improves on Gonew Classic by providing a more flexible
configuration and a vastly simpler code base. If you are migrating from Gonew
Classic, see the migration guide (MIGRATION.md).

About gonew
===========

Gonew is a tool for stubbing out new golang projects.

Documentation
=============

Detailed usage and configuration information is on [godoc.org][].

Help out
========

If you want to add more licence templates or improve existing templates
please [create a new issue][issues]. I'm always open to ideas regarding template
improvement. But be warned that I will scrutenize these ideas and reject some.
The generality of Gonew's templating system allows users to create and use
custom template sets. The guideline for altering Gonew's default templates is
this:

> Content generated by Gonew's default templates should follow all commonly
> accepted best-practices for their respective file types.

Any changes to gonew's templates should result in for greater adherence to this
principle.

Author
======

Bryan Matsuo (YotpoLtd)

Copyright & License
===================

Copyright (c) 2011, Bryan Matsuo.
All rights reserved.

Use of this source code is governed by a BSD-style license that can be
found in the LICENSE file.
